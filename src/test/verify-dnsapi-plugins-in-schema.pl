#!/usr/bin/perl

use strict;
use warnings;

use lib '../';

use JSON;

use PVE::Tools qw(dir_glob_foreach file_get_contents);

my $dnsapi_path = '../acme.sh/dnsapi';

die "cannot find dnsapi path '$dnsapi_path'!\n" if ! -d $dnsapi_path;

my $acmesh_plugins = [];
dir_glob_foreach($dnsapi_path, qr/dns_(\S+)\.sh/, sub {
    my ($file, $provider) = @_;
    push @$acmesh_plugins, $provider;
});

my $DNS_API_CHALLENGE_SCHEMA_FN = '../dns-challenge-schema.json';
my $defined_plugins = from_json(PVE::Tools::file_get_contents($DNS_API_CHALLENGE_SCHEMA_FN));

my ($missing_json_proposal, $missing_makefile_proposal) = ('', '');

my $ok = 1;
# first check for missing ones, delete from hash so we can easily see if a plug got removed/renamed
for my $provider (sort @$acmesh_plugins) {
    my $schema = delete $defined_plugins->{$provider};
    if (!defined($schema)) {
	$missing_json_proposal .= "    \"$provider\": {},\n";
	$missing_makefile_proposal .= "\tdnsapi/dns_${provider}.sh \\\n";
	$ok = 0;
    }
}

if (!$ok) {
    print STDERR "\nmissing plugins, add the following to the JSON schema:\n";
    print STDERR $missing_json_proposal;

    print STDERR "\nand to the Makefile:\n";
    print STDERR $missing_makefile_proposal;
}

my $printed_extra = 0;
for my $provider (sort keys %$defined_plugins) {
    print STDERR "\nplugins that got removed or renamed upstream:\n" if !$printed_extra;
    print STDERR "    $provider\n";
    $printed_extra = 1;
    $ok = 0;
}

die "\nERROR: schema not in sync with available plugins!\n\n" if !$ok;

print STDERR "OK: DNS challenge schema in sync with available plugins.\n";
exit(0);
